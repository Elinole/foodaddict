from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from app.models import Profil, Restaurant


class SearchForm(forms.Form):
    resto = forms.CharField(max_length=100)

class ConnexionForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur", max_length=30)
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)

class RegisterForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('username','password','email')

class ProfilForm(forms.ModelForm):
     class Meta():
         model = Profil
         fields = ('firstname','lastname', 'address', 'postal_code', 'city', 'avatar', 'is_restaurateur',)

class RestoForm(forms.ModelForm):
     class Meta():
         model = Restaurant
         fields = ('mail','name', 'address', 'postal_code', 'city', 'image',)