from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='accueil'),
    path('inscription', views.register, name='inscription'),
    path('connexion', views.connexion, name='connexion'),
    path('restaurant/<id_resto>', views.resto_details, name='restaurant'),
    path('mon-espace/<username>', views.monespace, name='mon-espace'),
    path('deconnexion', views.deconnexion, name='deconnexion'),
    path('admin-resto/<username>', views.restolist, name='admin-resto'),
    path('admin-resto/create/<username>', views.createresto, name='create-resto'),
    path('admin-resto/<username>/<name>', views.adminReadResto, name='read-resto-admin'),
    path('admin-resto/update/<name>', views.adminReadResto, name='update-resto'),
]