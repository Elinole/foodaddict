from django.shortcuts import render, redirect
from .forms import SearchForm, ConnexionForm
from django.contrib.auth import login, logout, authenticate
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import Http404
from app.models import Profil, Restaurant, Order, Dish, Bookmark
from app.forms import ProfilForm, RegisterForm, RestoForm
from django.contrib.auth.models import User
from django.core.paginator import Paginator


def home(request):

    restaurants_list = Restaurant.objects.all()
    paginator = Paginator(restaurants_list, 3)

    form = SearchForm(request.POST or None)
    if form.is_valid():
        resto = form.cleaned_data['resto']

        envoi = True

    page = request.GET.get('page')
    restaurants = paginator.get_page(page)

    return render(request, 'home.html', {'restaurants': restaurants})


def register(request):

    user = request.user
    if user.is_authenticated:
        return redirect("accueil")

    registered = False
    if request.method == 'POST':
        user_form = RegisterForm(data=request.POST)
        profile_form = ProfilForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            profile = profile_form.save(commit=False)
            print(profile.is_restaurateur)
            user.first_name = profile.firstname
            user.last_name = profile.lastname
            user.save()
            profile.user = user
            profile.username = user.username
            if 'avatar' in request.FILES:
                print('found it')
                profile.avatar = request.FILES['avatar']
            profile.save()
            registered = True
            messages.add_message(
                request, messages.SUCCESS, 'Vous êtes inscrit ! Vous pouvez vous connecter', extra_tags="success")
            return redirect('connexion')
        else:
            print(user_form.errors, profile_form.errors)
    else:
        user_form = RegisterForm()
        profile_form = ProfilForm()
    return render(request, 'register.html',
                  {'user_form': user_form,
                           'profile_form': profile_form,
                           'registered': registered})


def connexion(request):

    user = request.user
    if user.is_authenticated:
        return redirect("accueil")

    if request.method == "POST":
        form = ConnexionForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            # Nous vérifions si les données sont correctes
            user = authenticate(username=username, password=password)
            if user:  # Si l'objet renvoyé n'est pas None
                login(request, user)
                messages.add_message(
                    request, messages.SUCCESS, 'Vous êtes connecté !', extra_tags="success")
                return redirect("accueil")  # nous connectons l'utilisateur
            else:  # sinon une erreur sera affichée
                error = True
    else:
        form = ConnexionForm()

    return render(request, "login.html", locals())


def deconnexion(request):
    logout(request)
    messages.add_message(request, messages.SUCCESS,
                         'Vous êtes déconnecté !', extra_tags="success")
    return redirect(reverse(connexion))


def resto_details(request, id_resto):

    restaurant = Restaurant.objects.get(id=id_resto)
    dishes = Dish.objects.filter()

    return render(request, 'restos/resto_details.html', {'restaurant': restaurant, 'dishes': dishes})


@login_required
def monespace(request, username):
    profil = Profil.objects.get(username=username)
    user = User.objects.get(username=username)
    orders = Order.objects.filter(client=user)
    favouriteDishes = Bookmark.objects.filter(client=user)

    return render(request, 'monespace.html', {'profil': profil, 'orders': orders, 'favouriteDishes': favouriteDishes})


@login_required
def restolist(request, username):

    user = User.objects.get(username=username)
    restaurants_list = Restaurant.objects.filter(owner=user)
    paginator = Paginator(restaurants_list, 5)

    page = request.GET.get('page')
    restaurants = paginator.get_page(page)

    return render(request, 'restos/restolist.html', {'restaurants': restaurants})


@login_required
def createresto(request, username):

    user = User.objects.get(username=username)
    if request.method == 'POST':
        resto_form = RestoForm(data=request.POST)
        print(resto_form)
        if resto_form.is_valid():
            restaurant = resto_form.save(commit=False)
            restaurant.owner = user
            if 'image' in request.FILES:
                print('found it')
                restaurant.image = request.FILES['image']
                restaurant.save()
                messages.add_message(
                request, messages.SUCCESS, 'Vous avez ajouté votre restaurant !', extra_tags="success")
                return redirect('admin-resto', username)
            else:
                print(resto_form.errors)
    else:
        resto_form = RestoForm()
    return render(request, 'restos/restocreate.html',
                  {'resto_form': resto_form})

@login_required
def adminReadResto(request, username, name):

    user = User.objects.get(username=username)
    restaurant = Restaurant.objects.get(name=name)

    return render(request, 'restos/admin-restoread.html', {'resto': restaurant})

@login_required
def updateResto(request, username, name):

    user = User.objects.get(username=username)
    restaurant = Restaurant.objects.get(name=name)
    
    if request.method == 'POST':
        resto_form = RestoForm(data=request.POST)
        print(resto_form)
        if resto_form.is_valid():
            restaurant = resto_form.save(commit=False)
            restaurant.owner = user
            if 'image' in request.FILES:
                print('found it')
                restaurant.image = request.FILES['image']
                restaurant.save()
                messages.add_message(
                request, messages.SUCCESS, 'Vous avez modifié votre restaurant !', extra_tags="success")
                return redirect('admin-resto', username)
            else:
                print(resto_form.errors)
    else:
        resto_form = RestoForm()
    return render(request, 'restos/restocreate.html',
                  {'resto_form': resto_form})