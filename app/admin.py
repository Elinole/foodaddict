from django.contrib import admin
from app.models import Schedule, Restaurant, Bookmark, Profil, Ingredient, Category, Dish, Order

admin.site.register(Schedule)
admin.site.register(Restaurant)
admin.site.register(Bookmark)
admin.site.register(Profil)
admin.site.register(Ingredient)
admin.site.register(Category)
admin.site.register(Dish)
admin.site.register(Order)