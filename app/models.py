from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Profil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)  # La liaison OneToOne vers le modèle User
    firstname= models.CharField(max_length = 128)
    lastname= models.CharField(max_length = 128)
    username = models.CharField(max_length = 128 , unique=True)
    address = models.CharField(max_length=512)
    postal_code = models.CharField(max_length=16)
    city = models.CharField(max_length=128)
    avatar = models.ImageField(null=True, blank=True, upload_to="static/avatars_user/")
    is_restaurateur = models.BooleanField()

    def __str__(self):
        return "Profil de {0}".format(self.user.username)


class Schedule(models.Model):
    day = models.CharField(max_length=16)
    opening_hour = models.TimeField()
    closing_hour = models.TimeField()

class Category(models.Model):
    label = models.CharField(max_length = 128)

class Ingredient(models.Model):
    name = models.CharField(max_length=128)

class Dish(models.Model):
    name = models.CharField(max_length=256)
    ingredients = models.ManyToManyField(Ingredient)
    price = models.FloatField()
    cat = models.ManyToManyField(Category)
    quantity = models.IntegerField()
    img = models.ImageField()

    class Meta:
        ordering = ['name']

class Restaurant(models.Model):
    mail = models.CharField(max_length=128)
    name = models.CharField(max_length=256)
    address = models.CharField(max_length=512)
    postal_code = models.CharField(max_length=16)
    city = models.CharField(max_length=128)
    owner = models.ForeignKey(User, on_delete=models.CASCADE) 
    image = models.ImageField(null=True, blank=True, upload_to="static/restaurant/")
    categories = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True)
    dishes = models.ForeignKey(Dish, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        ordering = ['name']

class Bookmark(models.Model):
    dish = models.ManyToManyField(Dish)
    restaurant = models.ManyToManyField(Restaurant)
    client = models.ManyToManyField(User)

class Order(models.Model):
    dishes = models.ManyToManyField(Dish)
    date = models.DateTimeField(default=timezone.now, verbose_name='Date de la commande')
    client = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        ordering = ['date']