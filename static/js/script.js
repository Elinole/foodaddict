$(window).scroll(function() {     
    var scroll = $(window).scrollTop();
    if (scroll > 50) {
        $("#headernav").addClass("active");
    }
    else {
        $("#headernav").removeClass("active");
    }
});

$(document).ready(function(){ 
    $("#txtSearch").autocomplete({
        source: "/resto/search/",
        minLength: 2,
        open: function(){
            setTimeout(function () {
                $('.ui-autocomplete').css('z-index', 99);
            }, 0);
        }
      });
});
