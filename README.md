# Foodaddict

Site web permettant de commander des plats en ligne.

## Technologies utilisées

- Django 3.0.6

## Partage des tâches

- Julien : Modèles (Base de données)
- Elias : Vues (Contrôleurs)
- Tony : Templates (Front)

## Cas d'utilisation

Le site web regroupe tous les restaurateurs avec les plats qu'ils proposent. Les plats ont un prix et appartiennent à une ou plusieurs catégorie(s) (*eg. : Burgers / Pizzas / Boissons / Desserts / Japonais / Italien...*).
Un restaurateur peut donc s'inscrire sur le site et ajouter ses catégories et les plats qu'il souhaite proposer aux clients.

Les utilisateurs doivent s'inscrire sur le site pour pouvoir commander des plats. (En tant que visiteurs, ils peuvent tout de même voir les plats proposés par les restaurants.)

L'administrateur confirme ou infirme l'inscription du restaurateur.

## Perspectives d'amélioration

- Utilisation de Google Custom Search pour le moteur de recherche sur le site web